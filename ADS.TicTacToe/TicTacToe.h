#pragma once

#ifndef TicTacToe_h
#define TicTacToe_h

class TicTacToe {
private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;
public:
	TicTacToe();
	virtual void DisplayBoard();
	bool IsOver();
	char GetPlayerTurn();
	bool IsValidMove(int);
	virtual void Move(int);
	virtual void DisplayResult();
};
#endif