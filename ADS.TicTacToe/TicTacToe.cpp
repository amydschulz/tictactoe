#include <iostream>
#include "TicTacToe.h"

TicTacToe::TicTacToe() {
	for (int i = 0; i < 9; i++) {
		m_board[i] = ((i + 1) + 48);
	}
	this->m_numTurns = 0;
	m_playerTurn = 'X';
	m_winner = ' ';
}

void TicTacToe::DisplayBoard() {
	for (int i = 0; i < 9; i++) {
		if (i % 3 == 0) {
			std::cout << "\n";
		}
		std::cout << m_board[i] << " | ";
	}
}

bool TicTacToe::IsOver() {
	if (m_board[0] == m_board[1] && m_board[1] == m_board[2])
	{
		m_winner = m_playerTurn;
		return true;
	}

	else if (m_board[3] == m_board[4] && m_board[4] == m_board[5]) {
		m_winner = m_playerTurn;
		return true;
	}

	else if (m_board[6] == m_board[7] && m_board[7] == m_board[8]) {
		m_winner = m_playerTurn;
		return true;
	}
	else if (m_board[0] == m_board[3] && m_board[3] == m_board[6]) {
		m_winner = m_playerTurn;
		return true;
	}
	else if (m_board[0] == m_board[3] && m_board[3] == m_board[6]) {
		m_winner = m_playerTurn;
		return true;
	}
	else if (m_board[1] == m_board[4] && m_board[4] == m_board[7]) {
		m_winner = m_playerTurn;
		return true;
	}
	else if (m_board[0] == m_board[4] && m_board[4] == m_board[8]) {
		m_winner = m_playerTurn;
		return true;
	}
	else if (m_board[2] == m_board[4] && m_board[4] == m_board[6]) {
		m_winner = m_playerTurn;
		return true;
	}
	else if (m_board[0] != '1' && m_board[1] != '2' && m_board[2] != '3'
		&& m_board[3] != '4' && m_board[4] != '5' && m_board[5] != '6'
		&& m_board[6] != '7' && m_board[7] != '8' && m_board[8] != '9') {
		m_winner = 'T';
		return true;
	}
	else

		return false;
}

char TicTacToe::GetPlayerTurn() {
	return m_playerTurn;
}

bool TicTacToe::IsValidMove(int index) {
	if (index >= 1 && index <= 9) {
		if (this->m_board[index - 1] == (index + 48)) {

			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

void TicTacToe::Move(int index) {
	this->m_board[index - 1] = this->m_playerTurn;
	if (this->IsOver()) {
		return;
	}

	if (m_playerTurn == 'X') {
		m_playerTurn = 'O';
	}
	else {
		m_playerTurn = 'X';
	}
}

void TicTacToe::DisplayResult() {
	if (m_winner == 'X') {
		std::cout << "Player X Wins!" << "\n";
	}
	else if (m_winner == 'O') {
		std::cout << "Player O Wins!" << "\n";
	}
	else {
		std::cout << "TIE GAME - TRY AGAIN!" << "\n";
	}
}